/** ###################################################################
**     Filename  : Events.H
**     Project   : dev
**     Processor : MC9S12XS128MAA
**     Beantype  : Events
**     Version   : Driver 01.04
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 4/16/2010, 2:49 PM
**     Abstract  :
**         This is user's event module.
**         Put your event handler code here.
**     Settings  :
**     Contents  :
**         SPI_OnRxChar      - void SPI_OnRxChar(void);
**         SPI_OnTxChar      - void SPI_OnTxChar(void);
**         SPI_OnTxEmptyChar - void SPI_OnTxEmptyChar(void);
**         SPI_OnError       - void SPI_OnError(void);
**         SCI_OnError       - void SCI_OnError(void);
**         SCI_OnRxChar      - void SCI_OnRxChar(void);
**         SCI_OnTxChar      - void SCI_OnTxChar(void);
**
**     (c) Copyright UNIS, a.s. 1997-2008
**     UNIS, a.s.
**     Jundrovska 33
**     624 00 Brno
**     Czech Republic
**     http      : www.processorexpert.com
**     mail      : info@processorexpert.com
** ###################################################################*/

#ifndef __Events_H
#define __Events_H
/* MODULE Events */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "SCI.h"
#include "SPI.h"
#include "LED.h"

#pragma CODE_SEG DEFAULT

void SPI_OnRxChar(void);
/*
** ===================================================================
**     Event       :  SPI_OnRxChar (module Events)
**
**     From bean   :  SPI [SynchroSlave]
**     Description :
**         This event is called after a correct character is
**         received.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnTxChar(void);
/*
** ===================================================================
**     Event       :  SPI_OnTxChar (module Events)
**
**     From bean   :  SPI [SynchroSlave]
**     Description :
**         This event is called after a character is transmitted.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnTxEmptyChar(void);
/*
** ===================================================================
**     Event       :  SPI_OnTxEmptyChar (module Events)
**
**     From bean   :  SPI [SynchroSlave]
**     Description :
**         The event is called when an Empty character is sent (see
**         <Empty character> property for details about empty
**         character).
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void SPI_OnError(void);
/*
** ===================================================================
**     Event       :  SPI_OnError (module Events)
**
**     From bean   :  SPI [SynchroSlave]
**     Description :
**         This event is called when a channel error (not the error
**         returned by a given method) occurs. The errors can be
**         read using <GetError> method.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/* END Events */
#endif /* __Events_H*/

/*
** ###################################################################
**
**     This file was created by UNIS Processor Expert 3.00 [04.12]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
